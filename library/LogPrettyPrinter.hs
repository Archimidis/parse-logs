module LogPrettyPrinter (prettyLog, logEntry) where

import Text.PrettyPrint
import Data.Time (LocalTime)
import LogParser (Log, LogEntry(..), IP(..), Source(..), Product(..))

prettyLog :: Log -> Doc
prettyLog = foldl (\doc entry -> doc $+$ logEntry entry) empty

logEntry :: LogEntry -> Doc
logEntry le =
    logEntryTime (entryTime le)
    <+>
    logEntryIP (entryIP le)
    <+>
    logEntryProduct (entryProduct le)
    <+>
    logEntrySource (entrySource le)

logEntryTime :: LocalTime -> Doc
logEntryTime t = text (show t)

logEntryIP :: IP -> Doc
logEntryIP (IP a b c d) =
    text a <> char '.' <> text b <> char '.' <> text c <> char '.' <> text d

logEntryProduct :: Product -> Doc
logEntryProduct Mouse = text "mouse"
logEntryProduct Keyboard = text "keyboard"
logEntryProduct Monitor = text "monitor"
logEntryProduct Speakers = text "speakers"

logEntrySource :: Source -> Doc
logEntrySource Internet = text "internet"
logEntrySource Friend = text "friend"
logEntrySource NoAnswer = text "noanswer"
