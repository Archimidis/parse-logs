{-# LANGUAGE OverloadedStrings #-}

module LogParser where

import Data.ByteString.Char8 (ByteString, singleton)
import Data.Char (toLower)
import Data.Foldable (foldMap)
import Data.List (maximumBy)
import Data.Maybe (fromMaybe)
import Data.Monoid hiding (Product)
import Data.String
import Data.Time
import Text.ParserCombinators.Parsec

-----------------------
------- Domain --------
-----------------------

-- Replace String with Word8
data IP = IP String String String String deriving (Eq, Show)

data Product = Mouse | Keyboard | Monitor | Speakers deriving (Eq, Enum, Show)

productFromID :: Int -> Product
productFromID n = toEnum (n-1)

productToID :: Product -> Int
productToID p = fromEnum p + 1

data Source = Internet | Friend | NoAnswer deriving (Eq, Show)

data LogEntry =
    LogEntry { -- A local time contains the date and the time of the day.
               -- E.g. 2013-06-29 11:11:23
               entryTime :: LocalTime
             , entryIP :: IP
             , entryProduct :: Product
             , entrySource :: Source
             }
    deriving (Eq, Show)

instance Ord LogEntry where
    le1 <= le2 = entryTime le1 <= entryTime le2

type Log = [LogEntry]

type Sales = [(Product,Int)]

salesOf :: Product -> Sales -> Int
salesOf p xs = fromMaybe 0 $ lookup p xs

addSales :: Product -> Sales -> Sales
addSales p [] = [(p,1)]
addSales p ((x,n):xs) = if p == x then (x,n+1):xs
                                  else (x,n) : addSales p xs
mostSold :: Sales -> Maybe (Product, Int)
mostSold [] = Nothing
mostSold xs = Just $ maximumBy (\x y -> snd x `compare` snd y) xs

sales :: Log -> Sales
sales = foldr (addSales . entryProduct) []

-----------------------
------- Parser --------
-----------------------

parseIP :: GenParser Char st IP
parseIP = do
    [d1, d2, d3, d4] <- many digit `sepBy1` char '.'
    return (IP d1 d2 d3 d4)

timeParser :: GenParser Char st [String]
timeParser = do
    h <- count 2 digit
    _ <- char ':'
    m <- count 2 digit
    _ <- char ':'
    s <- count 2 digit
    return [h, m, s]


dateParser :: GenParser Char st [String]
dateParser = do
    y <- count 4 digit
    _ <- char '-'
    mm <- count 2 digit
    _ <- char '-'
    d <- count 2 digit
    return [y, mm, d]

dateTimeParser :: GenParser Char st LocalTime
dateTimeParser = do
    [y, mm, d] <- dateParser
    _ <- char ' '
    [h, m, s] <- timeParser
    return
        LocalTime { localDay = fromGregorian (read y) (read mm) (read d)
                  , localTimeOfDay = TimeOfDay (read h) (read m) (read s)
                  }

-- try introduces performance hit. Try to rewrite the parser using char.
productParser :: GenParser Char st Product
productParser =
    try (string "mouse" >> return Mouse)
    <|> (string "keyboard" >> return Keyboard)
    <|> (string "monitor" >> return Monitor)
    <|> (string "speakers" >> return Speakers)

sourceParser :: GenParser Char st Source
sourceParser =
        (string "internet" >> return Internet)
    <|> (string "friend" >> return Friend)
    <|> (string "noanswer" >> return NoAnswer)

logEntryParser :: GenParser Char st LogEntry
logEntryParser = do
    t <- dateTimeParser
    _ <- char ' '
    ip <- parseIP
    _ <- char ' '
    p <- productParser
    s <- option NoAnswer (char ' ' >> sourceParser)
    return $ LogEntry t ip p s

-- sepBy will cause troubles here since the last element of the separation will
-- be the emptry string
logParser :: GenParser Char st Log
logParser = logEntryParser `sepEndBy` newline

dateParser2 :: GenParser Char st [String]
dateParser2 = do
    d <- count 2 digit
    _ <- char '/'
    mm <- count 2 digit
    _ <- char '/'
    y <- count 4 digit
    return [y, mm ,d]

dateTimeParser2 :: GenParser Char st LocalTime
dateTimeParser2 = do
    [y, mm, d] <- dateParser2
    _ <- char ' '
    [h, m, s] <- timeParser
    return
        LocalTime { localDay = fromGregorian (read y) (read mm) (read d)
                  , localTimeOfDay = TimeOfDay (read h) (read m) (read s)
                  }

productParser2 :: GenParser Char st Product
productParser2 = productFromID . read . (:[]) <$> digit

logEntryParser2 :: GenParser Char st LogEntry
logEntryParser2 = do
    ip <- parseIP
    _ <- char ' '
    t <- dateTimeParser2
    _ <- char ' '
    p <- productParser2
    s <- option NoAnswer (char ' ' >> sourceParser)
    return $ LogEntry t ip p s

logParser2 :: GenParser Char st Log
logParser2 = logEntryParser2 `sepEndBy` newline

rowParser :: GenParser Char st LogEntry
rowParser = do
    let spaceSkip = many (tab <|> space)
        sepParser = spaceSkip >> char sepChar >> spaceSkip
    _ <- spaceSkip
    t <- dateTimeParser
    _ <- sepParser
    ip <- parseIP
    _ <- sepParser
    p <- productParser
    _ <- sepParser
    s <- sourceParser
    _ <- spaceSkip
    return $ LogEntry t ip p s

csvParser :: GenParser Char st Log
csvParser = many rowParser

merge :: Ord a => [a] -> [a] -> [a]
merge xs [] = xs
merge [] ys = ys
merge (x:xs) (y:ys) =
    if x <= y
        then x : merge xs (y:ys)
        else y : merge (x:xs) ys

-----------------------
----- RENDERING -------
-----------------------

sepChar :: Char
sepChar = ','

renderIP :: IP -> ByteString
renderIP (IP a b c d) =
       fromString a
    <> singleton '.'
    <> fromString b
    <> singleton '.'
    <> fromString c
    <> singleton '.'
    <> fromString d

renderEntry :: LogEntry -> ByteString
renderEntry le =
       fromString (show $ entryTime le)
    <> singleton sepChar
    <> renderIP (entryIP le)
    <> singleton sepChar
    <> fromString (fmap toLower $ show $ entryProduct le)
    <> singleton sepChar
    <> fromString (fmap toLower $ show $ entrySource le)

renderLog :: Log -> ByteString
renderLog = foldMap $ \le -> renderEntry le <> singleton '\n'
