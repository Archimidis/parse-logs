# LogParser

Following the tutorial [Parsing Log FIles in Haskell](https://www.schoolofhaskell.com/school/starting-with-haskell/libraries-and-frameworks/text-manipulation/attoparsec) found in *School of Haskell*, this project has the following goals:

* learn parsec.
* familiarize with QuickCheck.

## Development

``` sh
# Build the project.
stack build

# Run the test suite.
stack test

# Run the benchmarks.
stack bench

# Generate documentation.
stack haddock

# Execute demo.
stack exec parse-logs
```
