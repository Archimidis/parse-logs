import LogParser
import Text.ParserCombinators.Parsec
import qualified Data.ByteString.Char8 as BC

main :: IO ()
main =
    sequence_
        [ putStrLn "[1] Showing stats:" >> showStats
        , putStrLn "\n[2] Parse CSV logs:" >> parseAndRenderCSVLogs
        , putStrLn "\n[3] Render logs to CSV format:" >> renderToCSV
        ]

-----------------------
-------- Logs ---------
-----------------------

standardLogs :: String
standardLogs =
    unlines [ "2013-06-29 11:16:23 124.67.34.60 keyboard"
            , "2013-06-29 11:32:12 212.141.23.67 mouse"
            , "2013-06-29 11:33:08 212.141.23.67 monitor"
            , "2013-06-29 12:12:34 125.80.32.31 speakers"
            , "2013-06-29 12:51:50 101.40.50.62 keyboard"
            , "2013-06-29 13:10:45 103.29.60.13 mouse"
            , "2013-06-29 16:40:15 154.41.32.99 monitor internet"
            , "2013-06-29 16:51:12 103.29.60.13 keyboard internet"
            , "2013-06-29 17:13:21 121.95.68.21 speakers friend"
            , "2013-06-29 18:20:10 190.80.70.60 mouse noanswer"
            , "2013-06-29 18:51:23 102.42.52.64 speakers friend"
            , "2013-06-29 19:01:11 78.46.64.23 mouse internet"
            ]

format2Logs :: String
format2Logs =
    unlines [ "154.41.32.99 29/06/2013 15:32:23 4 internet"
            , "76.125.44.33 29/06/2013 16:56:45 3 noanswer"
            , "123.45.67.89 29/06/2013 18:44:29 4 friend"
            , "100.23.32.41 29/06/2013 19:01:09 1 internet"
            , "151.123.45.67 29/06/2013 20:30:13 2 internet"
            ]

csvLogs :: String
csvLogs =
    unlines [ "2013-06-29 11:16:23 , 124.67.34.60  , keyboard , noanswer"
            , "2013-06-29 11:32:12 , 212.141.23.67 , mouse    , noanswer"
            , "2013-06-29 11:33:08 , 212.141.23.67 , monitor  , noanswer"
            , "2013-06-29 12:12:34 , 125.80.32.31  , speakers , noanswer"
            , "2013-06-29 12:51:50 , 101.40.50.62  , keyboard , noanswer"
            , "2013-06-29 13:10:45 , 103.29.60.13  , mouse    , noanswer"
            , "2013-06-29 15:32:23 , 154.41.32.99  , speakers , internet"
            , "2013-06-29 16:40:15 , 154.41.32.99  , monitor  , internet"
            , "2013-06-29 16:51:12 , 103.29.60.13  , keyboard , internet"
            , "2013-06-29 16:56:45 , 76.125.44.33  , monitor  , noanswer"
            , "2013-06-29 17:13:21 , 121.95.68.21  , speakers , friend"
            , "2013-06-29 18:20:10 , 190.80.70.60  , mouse    , noanswer"
            , "2013-06-29 18:44:29 , 123.45.67.89  , speakers , friend"
            , "2013-06-29 18:51:23 , 102.42.52.64  , speakers , friend"
            , "2013-06-29 19:01:09 , 100.23.32.41  , mouse    , internet"
            , "2013-06-29 19:01:11 , 78.46.64.23   , mouse    , internet"
            , "2013-06-29 20:30:13 , 151.123.45.67 , keyboard , internet"
            ]

-----------------------
------ Scenarios ------
-----------------------

parseLogs :: Either ParseError Log
parseLogs = do
    xs <- parse logParser "standard logs" standardLogs
    ys <- parse logParser2 "format 2 logs" format2Logs
    return $ merge xs ys

parseAndRenderCSVLogs :: IO ()
parseAndRenderCSVLogs =
    case parse csvParser "csv logs" csvLogs of
        Left err -> putStrLn $ "Error while parsing CSV file: " ++ show err
        Right result -> mapM_ print result

renderToCSV :: IO ()
renderToCSV = do
    let r = parseLogs
    case r of
        Left err -> putStrLn $ "A parsing error was found: " ++ show err
        Right result -> BC.putStrLn $ renderLog result

showStats :: IO ()
showStats = do
    let r = parseLogs

    case r of
        Left err -> putStrLn $ "A parsing error was found: " ++ show err
        Right result ->
            case mostSold (sales result) of
                Nothing -> putStrLn "We didn't sell anything yet."
                Just (p,n) -> putStrLn $ "The product with more sales is " ++ show p
                                          ++ " with " ++ show n ++ " sales."
